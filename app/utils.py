from ast import literal_eval
from typing import Any


def to_list(data: Any) -> list:
    if not data:
        return []
    if isinstance(data, list):
        return data
    if isinstance(data, str):
        if data.startswith("[") and data.endswith("]"):
            try:
                return literal_eval(data)
            except SyntaxError:
                pass
        else:
            return [entry.strip() for entry in data.split(",")]
    raise ValueError(f"cannot convert {data} to list", data)
