from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from .api.main import router
from .core import logging
from .core.config import settings

app = FastAPI(
    title=settings.PROJECT_NAME,
    openapi_url=f"{settings.API_URL}/openapi.json",
    generate_unique_id_function=lambda route: f"{route.tags[0]}-{route.name}",
)

if settings.CORS_ENABLED:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin).strip("/") for origin in settings.CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app.include_router(router, prefix=settings.API_URL)
