import logging.config

from .config import settings

LOGGING_CONFIG = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "default": {
            "style": "{",
            "format": "{asctime} {levelname} {name} ## {message}",
            "datefmt": "%Y-%m-%dT%H:%M:%S",
        },
        "terse": {
            "style": "{",
            "format": "{relativeCreated} {message}",
        },
    },
    "handlers": dict(
        stdout={
            "formatter": "terse",
            "level": settings.LOGGING_LEVEL,
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stdout",
        },
        file={
            "formatter": "default",
            "level": settings.LOGGING_LEVEL,
            "class": "logging.handlers.WatchedFileHandler",
            "filename": settings.LOGGING_FILE,
        }
        if settings.LOGGING_FILE
        else {"class": "logging.NullHandler"},
    ),
    "loggers": {
        logger: {
            "handlers": ["file", "stdout"],
            "level": settings.LOGGING_LEVEL,
            "propagate": False,
        }
        for logger in [settings.PROJECT_NAME, "app", "__main__"]
    },
}

logging.config.dictConfig(LOGGING_CONFIG)
