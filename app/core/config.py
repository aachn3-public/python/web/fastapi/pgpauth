from pathlib import Path
from typing import Annotated, Any, Literal, TypeAlias

from pydantic import AnyUrl, BeforeValidator, computed_field, validator
from pydantic_settings import BaseSettings, SettingsConfigDict

from ..utils import to_list

Environment: TypeAlias = Literal["dev", "test", "prod"]
LoggingLevel: TypeAlias = Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="APP_",
        env_ignore_empty=True,
        extra="ignore",
    )

    PROJECT_NAME: str = "pgpauth"
    ENV: Environment = "dev"
    LOGGING_LEVEL: LoggingLevel | None = None
    LOGGING_FILE: Path | None = None

    @validator("LOGGING_LEVEL", always=True)
    def _set_default_logging_level(
        cls, fval: LoggingLevel | None, values: dict[str, Any]
    ) -> LoggingLevel:
        if fval:
            return fval
        if values.get("ENV", "dev") == "dev":
            return "DEBUG"
        if values["ENV"] == "test":
            return "INFO"
        return "WARNING"

    DOMAIN: str = "localhost"
    HTTPS_ENABLED: bool = False

    @computed_field
    @property
    def HOST(self) -> str:
        return "{}/{}".format("https" if self.HTTPS_ENABLED else "http", self.DOMAIN)

    API_VERSION: str | int = 1

    @computed_field
    @property
    def API_URL(self) -> str:
        return f"/api/v{self.API_VERSION}"

    CORS_ENABLED: bool = True
    CORS_ALLOWED: Annotated[list[AnyUrl] | str, BeforeValidator(to_list)] = []

    @computed_field
    @property
    def CORS_ORIGINS(self) -> list[AnyUrl]:
        return [self.HOST, *self.CORS_ALLOWED] if self.CORS_ENABLED else []


settings = Settings()
