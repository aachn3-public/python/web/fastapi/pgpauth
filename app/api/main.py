from __future__ import annotations

from fastapi import APIRouter

from .routes.main import routes

router = APIRouter()
for route in routes:
    router.include_router(
        route.router,
        prefix=getattr(route, "prefix", ""),
        tags=getattr(route, "tags", [getattr(route, "name", route.__name__)]),
    )
