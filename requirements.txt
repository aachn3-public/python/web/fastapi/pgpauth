starlette>=0.37.2
uvicorn>=0.29.0
pydantic>=1.10.14
pydantic-settings>=2.2.1
fastapi>=0.110.0
