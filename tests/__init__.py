import sys
from os import environ
from os.path import abspath, basename, dirname

environ.update(TEST="1")

BASE_DIR = dirname(dirname(abspath(__file__)))
sys.path = [BASE_DIR, *sys.path]
